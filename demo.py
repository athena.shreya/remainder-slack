from dataclasses import replace
import os
import requests
import schedule
import time
import json
import mysql.connector 
from remainder import events_dict, credential_dict
from dotenv import load_dotenv
from dateutil.tz import tzlocal
from datetime import datetime, timedelta
from slack_bolt import App
from google.oauth2.credentials import Credentials
from apiclient.discovery import build
from googleapiclient.errors import HttpError, Error

load_dotenv()

SLACK_APP_TOKEN = os.environ["SLACK_APP_TOKEN"]
SLACK_BOT_TOKEN = os.environ["SLACK_BOT_TOKEN"]
app = App(token=SLACK_BOT_TOKEN, name="Google-Calender-Bot")
events_list_day={}
conn = mysql.connector.connect(
    user='root',password='athena.s',host='localhost',database='remainder_db',port=3305)
mycursor = conn.cursor()

def get_all_events(user_id, now, max_time=None):
    #print(credential)
    all_events=[]
    SCOPES = ['https://www.googleapis.com/auth/calendar']
    credential = Credentials.from_authorized_user_file(user_id+'_token.json', SCOPES)
    API_NAME = os.environ["API_NAME"]
    API_VERSION = os.environ["API_VERSION"]
    API_KEY = os.environ["API_KEY"]
    service = build(API_NAME, API_VERSION, developerKey=API_KEY, credentials=credential)
    if max_time == None:
        max_time = now
    try:
        
    
        all_events=service.events().list(calendarId = 'primary', timeMin = now, timeMax = max_time,
                                            maxResults = 100, singleEvents=True, 
                                           orderBy = 'startTime').execute()
                        
        return all_events
                                
    except HttpError as e:
        print('error', e)
    

def events(all_events, user_id, current_datetime=None):
    
    events = all_events.get('items',[])
    for event in events:
        #if current_datetime != None and current_datetime == event['start'].get('dateTime', event['start'].get('date'))[:16]:                
        #    post_message([event['summary'],event['start'].get('dateTime', event['start'].get('date'))[:16]], user_id)
        #else:
        event_date_time = str(event['start'].get('dateTime', event['start'].get('date'))[0:16]).replace('T', ' ')
        print(event['summary'], event['start'].get('dateTime', event['start'].get('date')))
        if user_id in events_list_day.keys():
            
            events_list_day[user_id] += [(event['summary'], event_date_time)]
        else:
            events_list_day[user_id] = [(event['summary'], event_date_time)]
        message = 'Your event '+ event["summary"] +' at time' + event_date_time
        post_message(message, user_id)

def daily_event():

    print('daily_event')
    now = datetime.utcnow().isoformat() + 'Z'   
    max_time = (datetime.utcnow()+timedelta(days=1)).isoformat() + 'Z'
    mycursor = conn.cursor()
    mycursor.execute("select * from users;")
    results = mycursor.fetchall()
    for result in results: 
        all_events = get_all_events(result[0], now, max_time) 
        print(all_events)
        if all_events != []: 
            events(all_events, result[0])   
        else:
            post_message('Hey! Happy Morning, You dont have any event for the Day!',result[0])            

def check_event_update():

    print(events_list_day)
    now = datetime.utcnow().isoformat() +'Z'
    max = (datetime.utcnow()+timedelta(hours=1)).isoformat() + 'Z'
    
    #credential = json.loads(open('U033V7GNSAKtoken.json').read(), object_hook=json_object_conversion) 
   
    mycursor.execute("select * from users;")
    results = mycursor.fetchall()
    for result in results:
        all_events = get_all_events(result[0],now, max) 
        if all_events != []:
            events = all_events.get('items',[])
            for event in events:
                event_date_time = str(event['start'].get('dateTime', event['start'].get('date'))[0:16]).replace('T', ' ')
                check_event = (event['summary'], event_date_time)
                
                check_event_list_day = events_list_day[result[0]]
                if result[0] in events_list_day.keys():
                    if check_event not in check_event_list_day:
                        events_list_day[result[0]] += [check_event]
                else:
                    events_list_day[result[0]] = [(event['summary'], event_date_time)]
    

def remainder_event():

    current_datetime = str(datetime.now(tzlocal()).replace(microsecond=0).isoformat()[:16]).replace('T', ' ')
    for user_id, event in events_list_day.items():
        for each_event in event:
            print(each_event[1], current_datetime)
            if each_event[1] ==  current_datetime:
                message = 'Hey Alert!! Your event '+ each_event[0] +' at time' + each_event[1]
                post_message(message, user_id)
                event.remove(each_event)

def post_message(message,user):

    print(message)
    data1 = {
    'token': SLACK_BOT_TOKEN,
    'channel': user,
    'as_user': True,
    'text': message
    }
    res=requests.post(url='https://slack.com/api/chat.postMessage',data=data1)
    print('Response',res.text)

schedule.every().day.at("23:01").do(daily_event)
schedule.every(1).minutes.do(check_event_update)
schedule.every(1).minutes.do(remainder_event)
while True:
    schedule.run_pending() 
    time.sleep(0.2)