from  datetime import datetime, timedelta
import pickle
import datetime
import os
import mysql.connector 
from time import sleep
#from urllib.error import HTTPError
import requests, json
import getopt
import httplib2
import urllib
import sys
import schedule
import time
import webbrowser
import urllib.parse
from collections import namedtuple
from json import JSONEncoder
from dotenv import load_dotenv
from dateutil.tz import tzlocal
from datetime import datetime
from slack_bolt import Say
from slack_bolt import App
from apiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google_auth_oauthlib.flow import Flow
from google.auth.transport.requests import Request
from google.oauth2.credentials import Credentials
from googleapiclient.errors import HttpError, Error
# from webdriver_manager.chrome import ChromeDriverManager
# from selenium.webdriver.chrome.service import Service
# from selenium import webdriver

load_dotenv()
events_dict = {}
credential_dict ={}
SLACK_APP_TOKEN = os.environ["SLACK_APP_TOKEN"]
SLACK_BOT_TOKEN = os.environ["SLACK_BOT_TOKEN"]
app = App(token=SLACK_BOT_TOKEN, name="Google-Calender-Bot")
conn = mysql.connector.connect(
    user='root',password='athena.s',host='localhost',database='remainder_db',port=3305)
mycursor = conn.cursor()

def user_login(user_id):
    flow = None
    #print("credential check")
    credential = None
    APPLICATION_NAME = 'Google Calendar API Python '
    SCOPES = ['https://www.googleapis.com/auth/calendar']
    mycursor.execute("select 1 from users where %s", (user_id))
    results = mycursor.fetch()
    token = user_id + '_token.json'
    if results == 1:
        credential = Credentials.from_authorized_user_file(token, SCOPES)
            # If there are no (valid) credentials available, let the user log in.
    if not credential or not credential.valid:
        
        if credential and credential.expired and credential.refresh_token:
            credential.refresh(Request())
        else:
            
            flow = Flow.from_client_secrets_file(
                      'credentials.json', SCOPES, redirect_uri='urn:ietf:wg:oauth:2.0:oob')
            auth_url, _ = flow.authorization_url(prompt='consent')
            post_message(auth_url, user_id)

    return flow

def get_token(code,flow,user_id):

    flow.fetch_token(code=code)
    credential = flow.credentials
    with open(user_id+'_token.json', 'w') as token:
        token.write(credential.to_json())
    try:
        mycursor = conn.cursor()
        sql = "INSERT INTO users (userid, credential_file) VALUES (%s, %s)"
        val = (user_id, user_id+'_token.json')
        mycursor.execute(sql, val)
        conn.commit()
        mycursor.execute("select * from users;")
        result = mycursor.fetchall()
        
    except:
        print('Value Mismatch-Adding Failed')
    return credential

def json_object_conversion(token):
    return tuple('', token.keys())(*token.values())



        
def post_message(message,user):

    data1 = {
    'token': SLACK_BOT_TOKEN,
    'channel': user,
    'as_user': True,
    'text': message
    }
    res=requests.post(url='https://slack.com/api/chat.postMessage',data=data1)
    print(res.text)
        

