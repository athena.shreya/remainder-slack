import logging
import os
import re
import schedule
import time
from dotenv import load_dotenv
from slack_bolt import App
from slack_bolt.adapter.socket_mode import SocketModeHandler
from slack_bolt import Say
from remainder import *

load_dotenv()

SLACK_APP_TOKEN = os.environ["SLACK_APP_TOKEN"]
SLACK_BOT_TOKEN = os.environ["SLACK_BOT_TOKEN"]

app = App(token=SLACK_BOT_TOKEN, name="Google-Calender-Bot")
logger = logging.getLogger(__name__)
flow = None

# @app.event("message")
# def handle_message_events(body, logger):
#     logger.info(body)
#     return body['text']

@app.message(re.compile("^login"))
def get_login(message, say):
    #xprint('entered')
    global flow
    channel_type = message["channel_type"]
    if channel_type != "im":
        return
    
    dm_channel = message["channel"]
    user_id = message["user"]
    #print(say)
    print(dm_channel, user_id)
    flow = user_login(user_id)
    message_pass = "Login Initiated! " 
    logger.info(f"{message_pass} to user {user_id}")
    say(text= message_pass, channel=dm_channel, user=user_id)

@app.message(re.compile("^credential"))
def get_credential(message, say):
    #xprint('entered')
    global flow
    channel_type = message["channel_type"]
    if channel_type != "im":
        return
    dm_channel = message["channel"]
    user_id = message["user"]
    get_token(message['text'].split(' ')[1], flow, user_id)
    #daily_event(user_id)
    message_pass = "Successful " 
    logger.info(f"{message_pass} to user {user_id}")
    say(text= message_pass, channel=dm_channel, user=user_id)


def main():
    handler = SocketModeHandler(app, SLACK_APP_TOKEN)
    handler.start()
   

if __name__ == "__main__":
    main()