About
The Google Calendar integration with Slack gives you and your teams ease access to events remainder in the slack APP (Google-Calendar-Bot), where it generate trigger when an event is schedule for that time. This integration is an open source project, built an maintained by GitHub.

Installing the Google Calendar API integration for Slack
Requirements
This app officially supports calendar.google.com (which includes your personal gmail calendar) and Slack.com

Installation:
-   pip3 install --upgrade google-api-python-client google-auth-httplib2 google-auth-oauthlib
-   pip3 install requests
-   pip3 install python-dateutil
-   pip3 install docker
-   Needed MYSQL server ( Can download from official docker image: https://hub.docker.com/_/mysql)
-   Using Official docker image :
1) docker pull mysql
2) docker create volume slack-remainder
3) docker run --name some-mysql -e MYSQL_ROOT_PASSWORD=athena.s -e MYSQL_PASSWORD=password -e MYSQL_USER=username --platform linux/x86_64 -p   3306:3306 -v slack-remainder:/shared-volume -d mysql:latest

Getting Started
Subscribing 
At this point, your Slack and Google Calendar user accounts are not linked. To link the two accounts, authenticate to Google Calendar using the following command:
1) login : provides a google sign in link to login to your personal google account
2) credential <token-code> : provide the token-code, to authenticate the user.


